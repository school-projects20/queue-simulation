#ifndef TIME
#define TIME

typedef struct {
    int hour;
    int minute;
} Time;

int time_in_minutes(Time t);

#endif
