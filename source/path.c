#include <stdio.h>
#include <dirent.h>

#include "path.h"

void display_simulations()
{
    DIR *dir_list = opendir(".");
    if (dir_list != NULL) {
        struct dirent *dir;
        while ((dir = readdir(dir_list)) != NULL)
            if (!(dir->d_name[0] == '.'))
                printf("%s\n", dir->d_name);
        closedir(dir_list);
    }
}
