#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include "client.h"
#include "globals.h"
#include "time.h"

void init_client_list(Client_list *list)
{
    list->head = NULL;
}

void read_client_list(char *simulation_name, Client_list *list, int day)
{
    init_client_list(list);

    int client_nb;
    Time arrival;
    Time wait;
    Time service_start;
    Time service_end;

    char nom_dossier_jours[MAX_CHAR];
    strcpy(nom_dossier_jours, simulation_name);
    strcat(nom_dossier_jours, "/jours");

    char day_path[MAX_CHAR];
    strcpy(day_path, nom_dossier_jours) ;
    strcat(day_path, "/jour") ;
    char str_num_jour[MAX_CHAR];
    sprintf(str_num_jour, "%d", day) ;
    strcat(day_path, str_num_jour) ;
    strcat(day_path, ".txt") ;

    FILE *simulation = fopen(day_path, "r");

    if (simulation != NULL) {
        while (fscanf(simulation, "%d  %d %d   %d %d   %d %d   %d %d", &client_nb, &arrival.hour ,&arrival.minute,  &wait.hour, &wait.minute ,&service_start.hour , &service_start.minute, &service_end.hour, &service_end.minute) == 9) {
            Client *client;
            client = (Client *) malloc(sizeof(Client));

            client->client_nb = client_nb ;
            client->arrival = arrival;
            client->wait = wait;
            client->service_start = service_start ;
            client->service_end = service_end;
            if (service_end.hour < 17 || (service_end.hour < 18 &&  service_end.minute < 30))
                client->has_been_served = true;
            else
                client->has_been_served = false;
            insert_client(list, client);
        }
    }
    fclose(simulation);
}

void insert_client(Client_list *list, Client *client)
{
    Client *last = NULL;
    Client *current = list->head;

    while (current != NULL) {
        last = current;
        current = current->next;
    }
    if (last != NULL) {
        client->next = last->next;
        last->next = client;
    }
    else {
        client->next = list->head;
        list->head = client;
    }
}

void display_client(Client *client)
{
    printf("%d %dh%d %dh%d %dh%d %dh%d \n",client->client_nb ,client->arrival.hour ,client->arrival.minute ,client->wait.hour ,client->wait.minute ,client->service_start.hour ,client->service_start.minute ,client->service_end.hour ,client->service_end.minute);
}

void display_client_list(Client_list *list)
{
    Client *client = list->head;
    printf("\n");

    while (client != NULL) {
        display_client(client);
        client = client->next;
    }
}

void display_day(char *simulation_name, int day)
{
    Client_list lc;
    read_client_list(simulation_name, &lc, day);
    display_client_list(&lc);
}
