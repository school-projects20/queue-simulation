#include <stdlib.h>
#include <math.h>

#include "random.h"

float rng_exp(float lambda)
{
    int a = rand();
    float u = (float) a/RAND_MAX;
    float x = -log(1-u) / lambda;
    return x;
}

int rng_unif(int min, int max)
{
    int a = rand();
    float u = (float) a/RAND_MAX;
    float center = u * (max-min);
    int rounded_center = (int) center;
    int x = min + rounded_center;
    return x;
}
