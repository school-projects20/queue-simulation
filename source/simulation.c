#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>
#include <stdlib.h>
#include <stdbool.h>

#include "simulation.h"
#include "random.h"
#include "client.h"
#include "globals.h"
#include "time.h"

#define max(x, y) (x > y ? x : y)

void write_simulation(char *simulation_name, char *school_name, float lambda, int min_service_time, int max_service_time, int days_nb)
{
    write_info_file(simulation_name, school_name, lambda, min_service_time, max_service_time);

    char nom_dossier_jours[MAX_CHAR];
    strcpy(nom_dossier_jours, simulation_name);
    strcat(nom_dossier_jours, "/jours");
    mkdir(nom_dossier_jours, 0000777);

    char nom_dossier_statistiques[MAX_CHAR];
    strcpy(nom_dossier_statistiques, simulation_name);
    strcat(nom_dossier_statistiques, "/statistiques");
    mkdir(nom_dossier_statistiques, 0000777);

    for(int i=1; i<=days_nb; i++)
        write_a_simulated_day(simulation_name, lambda, min_service_time, max_service_time, i);
}

void write_info_file(char *simulation_name, char *school_name, float lambda, int min_service_time, int max_service_time)
{
    char info_filename[MAX_CHAR];
    strcpy(info_filename, simulation_name);
    strcat(info_filename, "/informations.txt");

    FILE *info = fopen(info_filename, "w");

    if(info != NULL) {
        fprintf(info, "Nom de l'établissement : %s\n", school_name);
        fprintf(info, "Lambda : %f\n", lambda);
        fprintf(info, "Time minimal de service : %d\n", min_service_time);
        fprintf(info, "Time maximal de service : %d\n", max_service_time);
    }
    fclose(info);
}

void write_a_simulated_day(char *simulation_name, float lambda, int min_service_time, int max_service_time, int day)
{
    char nom_dossier_jours[MAX_CHAR];
    strcpy(nom_dossier_jours, simulation_name);
    strcat(nom_dossier_jours, "/jours");

    char day_path[MAX_CHAR];
    strcpy(day_path, nom_dossier_jours) ;
    strcat(day_path, "/jour") ;
    char str_num_jour[MAX_CHAR];
    sprintf(str_num_jour, "%d", day) ;
    strcat(day_path, str_num_jour) ;
    strcat(day_path, ".txt") ;

    // A simulation for multiple days is done too fast, hence we need to change the random seed not only based on time, but also on the day number.
    srand(time(NULL) + day*1000);
    // Supposes business opens at 8am ie 480 minutes am.
    int hour = 480;
    int hour_checkout_freed = 480;
    int client_nb = 1;

    hour = hour + rng_exp(lambda);

    // Supposes business closes at 17pm ie 1020 minutes pm.
    while(hour < 1020) {
        hour_checkout_freed = write_client(hour, hour_checkout_freed, client_nb, day_path, min_service_time, max_service_time);
        client_nb ++;
        int time_before_next_client = rng_exp(lambda);
        hour = hour + time_before_next_client;
    }
}

int write_client(int arrival, int fin_service_precedent, int client_nb, char* chemin, int min_service_time, int max_service_time)
{
    FILE *file = fopen(chemin, "a");

    int service_end = 0;
    int wait;
    int service_start;

    if(file != NULL) {
        wait = max(fin_service_precedent - arrival, 0);
        service_start = max(fin_service_precedent, arrival);
        service_end = service_start + rng_unif(min_service_time, max_service_time);
        fprintf(file, "%d    %d %d   %d %d   %d %d   %d %d\n", client_nb, arrival/60, arrival%60, wait/60, wait%60, service_start/60, service_start%60, service_end/60, service_end%60);
    }
    fclose(file);

    // Return the hour at which the checkout is freed.
    return service_end;
}

void write_stats(char *simulation_name, int days_nb)
{
    for (int i=1; i<=days_nb; i++)
        write_stats_of_a_day(simulation_name, i);
    write_global_stats(simulation_name, days_nb);
}

void write_stats_of_a_day(char *simulation_name, int day)
{
    Client_list client_list;
    read_client_list(simulation_name, &client_list, day);

    Time arrival_times[MAX_ARRAY];
    Time service_start_times[MAX_ARRAY];
    Time service_end_times[MAX_ARRAY];

    Client *current;
    current = client_list.head;
    int clients_served = 0;
    int clients_not_served = 0;

    while (current != NULL) {
        arrival_times[clients_served] = current->arrival;
        service_start_times[clients_served] = current->service_start;
        service_end_times[clients_served] = current->service_end;

        if (current->has_been_served)
            clients_served++;
        else
            clients_not_served++;
        current = current->next;
    }

    char nom_dossier_statistiques[MAX_CHAR];
    strcpy(nom_dossier_statistiques, simulation_name);
    strcat(nom_dossier_statistiques, "/statistiques");

    char day_stat_path[MAX_CHAR];
    strcpy(day_stat_path, nom_dossier_statistiques);
    strcat(day_stat_path, "/jour");
    char str_num_jour[MAX_CHAR];
    sprintf(str_num_jour, "%d", day);
    strcat(day_stat_path, str_num_jour);
    strcat(day_stat_path, ".txt");

    Day_stats s;
    s.average_waiting_queue_length = average_queue_length(arrival_times, service_start_times, clients_served);
    s.max_waiting_queue_length = max_queue_length(arrival_times, service_start_times, clients_served,clients_not_served);
    s.average_clients_nb_per_hour = (float)clients_served/9;
    s.clients_not_served_ratio = (float)clients_not_served/(clients_served+clients_not_served);
    s.average_waiting_time = average_time_in(arrival_times, service_end_times, clients_served);

    FILE *day_stat_file = fopen(day_stat_path, "a");
    if (day_stat_file != NULL) {
        fprintf(day_stat_file, "%f\n", s.average_waiting_queue_length);
        fprintf(day_stat_file, "%f\n", s.max_waiting_queue_length);
        fprintf(day_stat_file, "%f\n", s.average_clients_nb_per_hour);
        fprintf(day_stat_file, "%f\n", s.clients_not_served_ratio);
        fprintf(day_stat_file, "%f\n", s.average_waiting_time);
    }
    fclose(day_stat_file);
}

void write_global_stats(char *simulation_name, int days_nb)
{
    Day_stats s;

    float average_waiting_queue_length = 0;
    float max_waiting_queue_length = 0;
    float average_clients_nb_per_hour = 0;
    float clients_not_served_ratio = 0;
    float average_waiting_time = 0;

    char nom_dossier_statistiques[MAX_CHAR];
    strcpy(nom_dossier_statistiques, simulation_name);
    strcat(nom_dossier_statistiques, "/statistiques");

    for (int i=0; i<days_nb; i++) {
        char day_stat_path[MAX_CHAR];
        strcpy(day_stat_path, nom_dossier_statistiques);
        strcat(day_stat_path, "/jour");
        char str_num_jour[MAX_CHAR];
        sprintf(str_num_jour, "%d", i+1);
        strcat(day_stat_path, str_num_jour);
        strcat(day_stat_path, ".txt");

        read_stats(&s, day_stat_path);
        average_waiting_queue_length += s.average_waiting_queue_length/days_nb;
        max_waiting_queue_length += s.max_waiting_queue_length/days_nb;
        average_clients_nb_per_hour += s.average_clients_nb_per_hour/days_nb;
        clients_not_served_ratio += s.clients_not_served_ratio/days_nb;
        average_waiting_time += s.average_waiting_time/days_nb;
    }

    char nom_fichier_stats_globales[MAX_CHAR];
    strcpy(nom_fichier_stats_globales, simulation_name);
    strcat(nom_fichier_stats_globales, "/statistiques/stats_moyennes.txt");

    FILE *file = fopen(nom_fichier_stats_globales, "a");

    if (file != NULL) {
        fprintf(file, "%f\n", average_waiting_queue_length);
        fprintf(file, "%f\n", max_waiting_queue_length);
        fprintf(file, "%f\n", average_clients_nb_per_hour);
        fprintf(file, "%f\n", clients_not_served_ratio);
        fprintf(file, "%f\n", average_waiting_time);
    }
    fclose(file);
}

void read_stats(Day_stats *s, char *stat_filename)
{
    FILE *stat_file = fopen(stat_filename, "r");

    if (stat_file != NULL) {
        float average_waiting_queue_length;
        float max_waiting_queue_length;
        float average_clients_nb_per_hour;
        float clients_not_served_ratio;
        float average_waiting_time;

        fscanf(stat_file, "%f", &average_waiting_queue_length);
        fscanf(stat_file, "%f", &max_waiting_queue_length);
        fscanf(stat_file, "%f", &average_clients_nb_per_hour);
        fscanf(stat_file, "%f", &clients_not_served_ratio);
        fscanf(stat_file, "%f", &average_waiting_time);
        s->average_waiting_queue_length = average_waiting_queue_length;
        s->max_waiting_queue_length = max_waiting_queue_length;
        s->average_clients_nb_per_hour = average_clients_nb_per_hour;
        s->clients_not_served_ratio = clients_not_served_ratio;
        s->average_waiting_time = average_waiting_time;
    }
    fclose(stat_file);
}

float average_time_in(Time *arrival_times, Time *service_end_times, int clients_served)
{
    float total_waiting_time = 0.0;
    for(int i=0; i<clients_served; i++)
        total_waiting_time = total_waiting_time + (float) (time_in_minutes(service_end_times[i]) - time_in_minutes(arrival_times[i]));
    return total_waiting_time/clients_served;
}

float max_queue_length(Time *arrival_times, Time *service_end_times, int clients_served, int clients_not_served)
{
    int first_in_queue = 0;
    int last_in_queue = 1;
    int actual_queue_length = 1;
    int max_queue_length = 1;

    while (last_in_queue != clients_served+clients_not_served) {
        if (time_in_minutes(service_end_times[first_in_queue]) > time_in_minutes(arrival_times[last_in_queue])) {
            actual_queue_length++;
            last_in_queue++;
        }

        else if (time_in_minutes(service_end_times[first_in_queue]) < time_in_minutes(arrival_times[last_in_queue])) {
            actual_queue_length--;
            first_in_queue++;
        }

        else if (time_in_minutes(service_end_times[first_in_queue]) == time_in_minutes(arrival_times[last_in_queue])) {
            first_in_queue++;
            last_in_queue++;
        }

        max_queue_length = max(max_queue_length, actual_queue_length);
    }
    return (float) max_queue_length;
}

float average_queue_length(Time *arrival_times, Time *service_end_times, int clients_served)
{
    int first_in_queue = 0;
    int last_in_queue = 1;
    int actual_queue_length = 1;
    float average_waiting_queue_length = 0;
    int time = time_in_minutes(arrival_times[0]);

    while(last_in_queue != clients_served) {
        if (time_in_minutes(service_end_times[first_in_queue]) > time_in_minutes(arrival_times[last_in_queue])) {
            average_waiting_queue_length = average_waiting_queue_length + actual_queue_length * (time_in_minutes(arrival_times[last_in_queue]) - time);
            time = time_in_minutes(arrival_times[last_in_queue]);
            actual_queue_length++;
            average_waiting_queue_length = average_waiting_queue_length + actual_queue_length * (time_in_minutes(service_end_times[first_in_queue]) - time);
            time = time_in_minutes(service_end_times[first_in_queue]);
            last_in_queue++;
        }

        else if (time_in_minutes(service_end_times[first_in_queue]) < time_in_minutes(arrival_times[last_in_queue])) {
            average_waiting_queue_length = average_waiting_queue_length + actual_queue_length * (time_in_minutes(service_end_times[first_in_queue]) - time);
            time = time_in_minutes(service_end_times[first_in_queue]);
            actual_queue_length--;
            average_waiting_queue_length = average_waiting_queue_length + actual_queue_length * ( time_in_minutes(arrival_times[last_in_queue]) - time);
            time = time_in_minutes(arrival_times[last_in_queue]);
            first_in_queue++;

        }

        else if (time_in_minutes(service_end_times[first_in_queue]) == time_in_minutes(arrival_times[last_in_queue])) {
            average_waiting_queue_length = average_waiting_queue_length + actual_queue_length * (time_in_minutes(arrival_times[last_in_queue]) - time);
            time = time_in_minutes(arrival_times[last_in_queue]);
            first_in_queue++;
            last_in_queue++;
        }
    }

    average_waiting_queue_length = average_waiting_queue_length/570; // 570 minutes = 9 and a half hours
    return average_waiting_queue_length;
}

void display_day_stats(char *simulation_name, int day)
{
    char stats_folder_name[MAX_CHAR];
    strcpy(stats_folder_name, simulation_name);
    strcat(stats_folder_name, "/statistiques");

    char str_num_jour[MAX_CHAR];
    sprintf(str_num_jour, "%d", day);
    strcat(stats_folder_name, str_num_jour);
    strcat(stats_folder_name, ".txt");

    Day_stats s;
    read_stats(&s, stats_folder_name);

    printf("Average waiting queue length = %f\nMax waiting queue length = %f\nAverage cients number per hour = %f\nClients not served ratio = %f\nAverage waiting time = %f\n", s.average_waiting_queue_length, s.max_waiting_queue_length, s.average_clients_nb_per_hour, s.clients_not_served_ratio, s.average_waiting_time);
}

void display_average_stats(char *simulation_name)
{
    char stats_folder_name[MAX_CHAR];
    strcpy(stats_folder_name, simulation_name);
    strcat(stats_folder_name, "/statistiques/stats_moyennes.txt");

    Day_stats s;
    read_stats(&s, stats_folder_name);

    printf("Average waiting queue length = %f\nMax waiting queue length = %f\nAverage cients number per hour = %f\nClients not served ratio = %f\nAverage waiting time = %f\n", s.average_waiting_queue_length, s.max_waiting_queue_length, s.average_clients_nb_per_hour, s.clients_not_served_ratio, s.average_waiting_time);
}
