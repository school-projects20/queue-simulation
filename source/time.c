#include "time.h"

int time_in_minutes(Time t)
{
    int min;
    min = t.minute + 60*(t.hour);
    return min;
}
