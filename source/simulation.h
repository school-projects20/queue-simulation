#ifndef SIMULATION
#define SIMULATION

#include "client.h"
#include "time.h"

void write_simulation(char *simulation_name, char *school_name, float lambda, int min_service_time, int max_service_time, int days_nb);
void write_info_file(char *simulation_name, char *school_name, float lambda, int min_service_time, int max_service_time);
void write_a_simulated_day(char *simulation_name, float lambda,  int min_service_time, int max_service_time, int day);
int write_client(int arrival, int fin_service_precedent, int client_nb, char* chemin, int min_service_time, int max_service_time);

void write_stats(char *simulation_name, int days_nb);
void write_stats_of_a_day(char *simulation_name, int day);
void write_global_stats(char *simulation_name, int days_nb);
void read_stats(Day_stats *s, char *stat_filename );

int time_in_minutes(Time t);
float average_time_in(Time *tab_deb, Time *tab_fin, int nb_client);
float max_queue_length(Time *tab_deb, Time *tab_fin, int nb_client, int nb_client_nonserv);
float average_queue_length(Time *tab_deb, Time *tab_fin, int nb_client);

void display_day_stats(char *simulation_name, int day);
void display_average_stats(char *simulation_name);

#endif
