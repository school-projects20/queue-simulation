#ifndef INTERFACE
#define INTERFACE

void main_menu();
void set_settings_menu(char *school_name, float *lambda, int *min_service_time, int *max_service_time);
void create_simulation_menu(char *school_name, float lambda, int min_service_time, int max_service_time);
void observation_menu();

// void menu_comparer();
// void menu_graphe();

#endif
