#ifndef RANDOM
#define RANDOM

float rng_exp(float lambda);
int rng_unif(int min, int max);

#endif
