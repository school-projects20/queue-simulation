#ifndef STRUCTURE_CLIENTS
#define STRUCTURE_CLIENTS

#include "time.h"

typedef struct client {
    int client_nb;
    Time arrival;
    Time wait;
    Time service_start;
    Time service_end;
    bool has_been_served;
    struct client *next;
} Client;

typedef struct {
    Client *head;
} Client_list;

typedef struct {
    float average_waiting_queue_length;
    float max_waiting_queue_length;
    float average_clients_nb_per_hour;
    float clients_not_served_ratio;
    float average_waiting_time;
} Day_stats;

void init_client_list(Client_list *list);
void read_client_list(char *simulation_name, Client_list *list, int day);
void insert_client(Client_list *list, Client *client);
void display_client(Client *client);
void display_client_list(Client_list *list);
void display_day(char *simulation_name, int day);

#endif
