#include <stdio.h>
#include <stdbool.h>
#include <sys/stat.h>

#include "interface.h"
#include "simulation.h"
#include "path.h"
#include "random.h"
#include "globals.h"

#define EXPLICATION_LAMBDA "Cette simulation utilise un paramètre lambda permettant de modéliser le temps séparant l'arrivée de deux clients consécutifs.\n\nCelui-ci est modélisé par une variable aléatoire définie comme suit:\n-ln(1-X)/lambda\noù X est une variable aléatoire uniforme sur [0, 1[.\n\nDans les faits, cela donne le tableau indicatif suivant:\n\n"

#define EXPLICATION_TMPSRV "Cette simulation utilise deux paramètres supplémentaires permettant de modéliser le temps de service d'un client au guichet ou en caisse.\nCelui-ci est modélisé par une variable aléatoire uniforme sur l'ensemble des entiers compris (largement) entre le temps minimal de service et le temps maximal de service.\n\n"

void main_menu()
{
    printf("Welcome to our queue simulation software.\n");

    char school_name[MAX_CHAR];
    float lambda;
    int min_service_time;
    int max_service_time;

    bool settings_set = false;

    int choice;
    while (true) {
        printf("1 Change settings\n2 Simulate\n3 See simulation\n4 Compare simulations\n5 Display a queue\n6 Exit\n");

        scanf("%d", &choice);
        switch(choice)
        {
            case 1:
                set_settings_menu(school_name, &lambda, &min_service_time, &max_service_time);
                settings_set = true;
                break;
            case 2:
                if (!settings_set) {
                    printf("Settings are not set. Please set settings before proceeding.\n");
                    set_settings_menu(school_name, &lambda, &min_service_time, &max_service_time);
                    settings_set = true;
                }
                create_simulation_menu(school_name, lambda, min_service_time, max_service_time);
                break;
            case 3:
                observation_menu();
                break;
            case 4:
                printf("Feature not available.\n");
                // menu_comparer();
                break;
            case 5:
                printf("Feature not available.\n");
                // menu_graphe();
                break;
            case 6:
                goto end_main_menu;
                break;
            default:
                printf("Invalid input. Try again.\n");
                break;
        }
    }
    end_main_menu:
    printf("See you soon!\n");
}

void set_settings_menu(char *school_name, float *lambda, int *min_service_time, int *max_service_time)
{
    printf("Business name: ");
    scanf("%s", school_name);

    printf("Lambda: ");
    scanf("%f", lambda);

    printf("Minimal service time (int): ");
    scanf("%d", min_service_time);

    printf("Maximal service time (int): ");
    scanf("%d", max_service_time);

    printf("\n");
}

void create_simulation_menu(char *school_name, float lambda, int min_service_time, int max_service_time)
{
    char simulation_name[MAX_CHAR];

    printf("Enter simulation name. (should not start with a \".\")\n");
    scanf("%s", simulation_name);
    mkdir(simulation_name, 0000777);

    int days_nb;
    printf("How many days do you want to simulate?\n");
    scanf("%d", &days_nb);

    write_simulation(simulation_name, school_name, lambda, min_service_time, max_service_time, days_nb);
    write_stats(simulation_name, days_nb);

    printf("Simulation has been done.\n\n");
}

void observation_menu()
{
    int choice;
    int day;

    printf("Here are existing simulations.\n");
    display_simulations();
    printf("\n");

    char simulation_name[MAX_CHAR];
    printf("Which one do you want to see?\n");
    scanf("%s", simulation_name);

    while (true) {
        printf("1 Display day info\n2 Display day stats\n3 Display average stats\n4 Back to main menu\n");
        scanf("%d", &choice);

        switch(choice) {
            case 1:
                printf("Which day do you want to see?\n");
                scanf("%d", &day);
                display_day(simulation_name, day);
                break;
            case 2:
                printf("Which day stats do you want to see?\n");
                scanf("%d", &day);
                display_day_stats(simulation_name, day);
                break;
            case 3:
                display_average_stats(simulation_name);
                break;
            case 4:
                goto end_observation_menu;
                break;
            default:
                printf("Invalid input. Try again.\n");
                break;
        }
    }
    end_observation_menu:
}

/*

void menu_graphe()
{
    printf("Vous avez décidé de visualiser une file d'attente. Voici les simulations actuelles:\n");
    display_simulations();
    printf("\n Dans quelle simulation la file d'attente que vous souhaitez visualiser se trouve-t-elle ?\n");
    scanf(" %s", simulation_name);

    printf("De quel day souhaitez-vous observer la liste d'attente ?\n");
    printf("Jour numéro: ");
    int num_jour;
    scanf("%d", &num_jour);

    printf("Voici le graphe du day %d de la simulation %s.", num_jour, simulation_name);

    graphe_une_journee(num_jour);
}

*/
