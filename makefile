CC = gcc
EXEC = bin/main
SRC = $(wildcard source/*.c)
OBJ = $(patsubst source/%.c, objects/%.o, $(SRC))
LDFLAGS = -lm -g -Wall -std=c17

all: $(EXEC)

objects/%.o: source/%.c
	mkdir objects -p
	$(CC) -c $< -o $@ $(LDFLAGS)

$(EXEC): $(OBJ)
	mkdir bin -p
	$(CC) -o $(EXEC) $(OBJ) $(LDFLAGS)

run: $(EXEC)
	./$(EXEC)

clean:
	rm -f $(OBJ)

rmproper:
	rm -r objects
	rm -r bin
