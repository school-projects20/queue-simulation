# Queue Simulation

*I did this program for a school project. This repository is only for demonstration purposes.*

This program simulates queues in a store.

For service times of a few minutes, a lambda lower than 0.2 is recommended to avoid excessively long waiting times (>24h).

For the display of waiting lists (plot), a lambda between 0.05 and 0.1 is recommended. The service time should be in the order of a few minutes.

## Known flows

This program does not handle unexpected inputs. Please do not ask to use a non-existing file. Please do not use a simulation name that was already used.

This program really lacks a clear architecture. Coupling is too high. I leave it as is to remember to not use C for this type of project, and to remember to have clearer architecture.
